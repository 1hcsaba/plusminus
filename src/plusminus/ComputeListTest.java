package plusminus;

/*
*
* feladat: https://github.com/zebalu/eltegyak18/blob/master/09/Plusminus.md
* neptun kód: u7jxgs
*
* */

import org.junit.jupiter.api.Test;

import java.security.InvalidParameterException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static plusminus.ComputeList.formatDouble;

class ComputeListTest {
    private Integer[] arr_empty = {};
    private Integer[] arr_0 = {0};
    private Integer[] arr_1 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    private Integer[] arr_2 = {3,4,3,4,5,2,4,5,6,2,2,22,2,423,4,5432,234,4,23,2,423,423,4,234,234,23,1,34,231,4,654,6567,67,867,867,256,35,3534,234,513,5,34,25};
    private Integer[] arr_3 = {-1, -2, -3, -4, -5, 0, 1, 1, 2};


    private ComputeList testList0 = new ComputeList(arr_0);
    private ComputeList testList1 = new ComputeList(arr_1);
    private ComputeList testList2 = new ComputeList(arr_2);
    private ComputeList testList3 = new ComputeList(arr_3);

    @Test
    void emptyList() {
        assertThrows(InvalidParameterException.class, () -> {
            new ComputeList(arr_empty);
        });
    }

    @Test
    void min() {
        assertEquals(testList0.min(), 0);
        assertEquals(testList1.min(), 0);
        assertEquals(testList2.min(), 1);
        assertEquals(testList3.min(), -5);
    }

    @Test
    void max() {
        assertEquals(testList0.max(), 0);
        assertEquals(testList1.max(), 10);
        assertEquals(testList2.max(), 6567);
        assertEquals(testList3.max(), 2);
    }

    @Test
    void length() {
        assertEquals(testList0.length(), 1);
        assertEquals(testList1.length(), 11);
        assertEquals(testList2.length(), 43);
        assertEquals(testList3.length(), 9);
    }

    @Test
    void average() {
        assertEquals(formatDouble(testList0.average()), formatDouble(0));
        assertEquals(formatDouble(testList1.average()), formatDouble(5));
        assertEquals(formatDouble(testList2.average()), formatDouble(498.953488372));
        assertEquals(formatDouble(testList3.average()), formatDouble(-1.22222222222));
    }

    @Test
    void deviation() {
        assertEquals(formatDouble(testList0.deviation()), formatDouble(0));
        assertEquals(formatDouble(testList1.deviation()), formatDouble(2.72727272727));
        assertEquals(formatDouble(testList2.deviation()), formatDouble(694.945375879));
        assertEquals(formatDouble(testList3.deviation()), formatDouble(2.02469135802));
    }

    @Test
    void modus() {
        Integer[] res2 = {4};
        Integer[] res3 = {1};
        assertArrayEquals(testList0.modus(), arr_0);
        assertArrayEquals(testList1.modus(), arr_1);
        assertArrayEquals(testList2.modus(), res2);
        assertArrayEquals(testList3.modus(), res3);
    }

    @Test
    void median() {
        assertEquals(formatDouble(testList0.median()), formatDouble(0));
        assertEquals(formatDouble(testList1.median()), formatDouble(5));
        assertEquals(formatDouble(testList2.median()), formatDouble(23));
        assertEquals(formatDouble(testList3.median()), formatDouble(-1));
    }
}