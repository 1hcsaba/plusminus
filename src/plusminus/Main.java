package plusminus;

/*
 *
 * feladat: https://github.com/zebalu/eltegyak18/blob/master/09/Plusminus.md
 * neptun kód: u7jxgs
 *
 * */

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        try {
            if (args.length == 0) {

                System.out.println("----------------------------- v1.0 -----------------------------------------------------");
                System.out.println("| Kezdj el egész számokat gepelni, majd ha ugy gondolod eleg, kuldj egy '.' karaktert.  |");
                System.out.println("----------------------------------------------------------------------------------------");

                ArrayList<Integer> nums = new ArrayList<>();

                try(BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
                    String line = br.readLine();
                    int val;

                    while (!line.equals(".")) {
                        try {
                            val = Integer.parseInt(line);
                            nums.add(val);
                        } catch (NumberFormatException e) {
                            System.out.println("[warning] szamot csak");
                        } finally {
                            line = br.readLine();
                        }
                    }
                }

                ComputeList computeList = new ComputeList(nums);
                System.out.println(computeList);
            } else if (args.length == 1) {
                ComputeList computeList = new ComputeList(args[0]);
                System.out.println(computeList);
            } else {
                throw new IllegalArgumentException("Tul sok argumentum");
            }
        } catch (Exception e) {

            e.printStackTrace();

        }
    }
}
