package plusminus;

/*
 *
 * feladat: https://github.com/zebalu/eltegyak18/blob/master/09/Plusminus.md
 * neptun kód: u7jxgs
 *
 * */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.*;

public class ComputeList {
    private ArrayList<Integer> list = new ArrayList<>();

    public ComputeList(List<Integer> list) throws InvalidParameterException {
        if(list.size() == 0) {
            throw new InvalidParameterException("Ures halmaz nem");
        } else {
            this.list.addAll(list);
        }

    }
    public ComputeList(Integer[] list) throws InvalidParameterException {
        if(list.length == 0) {
            throw new InvalidParameterException("Ures halmaz nem");
        } else {
            this.list.addAll(Arrays.asList(list));
        }

    }

    public ComputeList(String fileName) throws IOException {
        this.loadFile(fileName);
    }

    private void loadFile(String fileName) throws IOException {
        File f = new File(getClass().getResource(fileName).getPath());

        if (!f.exists() || !f.isFile()) {
            System.err.println(f.getAbsolutePath());
            System.err.println("Provide a filename to an exisiting file");
            throw new IllegalStateException("Program was started with an incorrect parameter");
        }

        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(f))) {
            String currLine;
            while ((currLine = bufferedReader.readLine()) != null) {
                this.list.add(Integer.parseInt(currLine));
            }
        }
    }

    public int min() {
        Optional<Integer> possibleMinVal = this.list.stream().min(Integer::compare);
        if(possibleMinVal.isPresent()) {
            return possibleMinVal.get();
        } else {
            throw new NoSuchElementException("Nem letezik minimuma a listanak.");
        }
    }

    public int max() {
        Optional<Integer> possibleMaxVal = this.list.stream().max(Integer::compare);
        if(possibleMaxVal.isPresent()) {
            return possibleMaxVal.get();
        } else {
            throw new NoSuchElementException("Nem letezik maximuma a listanak.");
        }
    }

    public int length() {
        return this.list.size();
    }

    public double average() {
        double n = (double) this.length();
        return this.list.stream().mapToInt(Integer::intValue).sum() / n;
    }

    /*private double nthRoot(double value, double n) {
        return Math.exp(Math.log(value) * (1.0 / n));
    }*/

    public double deviation() {
        double A = this.average();
        double sum = this.list.stream().mapToDouble(x -> (Math.abs(x - A))).sum();
        return sum / this.length();
    }

    public Integer[] modus() {
        /* https://stackoverflow.com/a/16112490 */
        HashMap<Integer, Integer> freqs = new HashMap<>();
        for (Integer d : this.list) {
            Integer freq = freqs.get(d);
            if(freq == null) {
                freqs.put(d, 1);
            } else {
                freqs.replace(d, freq + 1);
            }
        }
        List<Integer> mode = new ArrayList<>();
        List<Integer> frequency = new ArrayList<>();
        List<Integer> values = new ArrayList<>();

        for (Map.Entry<Integer, Integer> entry : freqs.entrySet()) {
            frequency.add(entry.getValue());
            values.add(entry.getKey());
        }
        double max = Collections.max(frequency);

        for(int i = 0; i < frequency.size(); i++) {
            double val = frequency.get(i);
            if(max == val)  {
                mode.add(values.get(i));
            }
        }
        return mode.toArray(new Integer[0]);
    }

    public static final String formatDouble(double a) {
        return String.format("%.4f", a);
    }

    public double median() {
        ArrayList<Integer> tmp = new ArrayList<>(this.list);
        tmp.sort(Integer::compareTo);

        int middle = tmp.size() / 2;

        if (tmp.size() % 2 == 1) {
            return tmp.get(middle);
        } else {
            return (tmp.get(middle - 1) + tmp.get(middle)) / 2.0;
        }
    }

    @Override
    public String toString() {
        ArrayList<Integer> tmp = new ArrayList<>(this.list);
        tmp.sort(Integer::compareTo);
        StringBuilder sb = new StringBuilder();
        sb.append("A szamlista novekvo sorrendben: ");
        for(int el : tmp) {
            sb.append(el);
            sb.append(" ");
        }
        return sb.toString();
    }
}
